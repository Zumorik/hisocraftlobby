package net.zumorik.hiso.commands;

import net.md_5.bungee.api.ChatColor;
import net.zumorik.hiso.inventory.LoginInventory;
import net.zumorik.hiso.listeners.ParkourTest;
import net.zumorik.hiso.location.FireworkRunnable;
import net.zumorik.hiso.main.Lobby;
import net.zumorik.hiso.util.ItemCreator;
import net.zumorik.hiso.util.Title;
import net.zumorik.main.api.CoreAPI;
import net.zumorik.main.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;

public class LobbyCommands implements CommandExecutor {

    private Lobby lobby;

    public LobbyCommands(Lobby lobby){
        this.lobby = lobby;
    }

    private String prefix = "&e&lHiso&f&lCraft &7:";

    public static boolean enabled = true;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {




        if(cmd.getName().equalsIgnoreCase("hiso")){
            if(!(sender instanceof Player)){
                sender.sendMessage(colo("&cYou must be a player to run this command."));
                return true;
            }

            Player p = (Player) sender;

            if(CoreAPI.getRanks().hasBasicSystemPermissions(p)) {

                if (args.length <= 0) {
                    p.sendMessage(colo(prefix + " &cPlease specify an action."));
                    return true;
                }

                if (args[0].equalsIgnoreCase("setloc")) {
                    if (args.length <= 1) {
                        p.sendMessage(colo(prefix + "&c Please speficy a location name."));
                        return true;
                    }

                    lobby.getLocationHandler().createLocation(args[1], p);
                    p.sendMessage(colo(prefix + " &bLocation " + args[1] + " has been created."));
                    return true;
                }

                if(args[0].equalsIgnoreCase("test")){
                    ParkourTest test = new ParkourTest(lobby);
                    test.play(p);
                }

                if (args[0].equalsIgnoreCase("build")) {

                    if (lobby.getRegisterListeners().getBlocks().getBuilders().contains(p)) {
                        lobby.getRegisterListeners().getBlocks().getBuilders().remove(p);
                        p.sendMessage(colo(prefix + " &cYou have been removed from the builder list."));
                        return true;
                    } else {
                        lobby.getRegisterListeners().getBlocks().getBuilders().add(p);
                        p.sendMessage(colo(prefix + " &aYou have been added to the builder list."));
                    }

                    return true;
                }

                if (args[0].equalsIgnoreCase("block")) {
                    if(!CoreAPI.getRanks().hasSuperiorSystemPermissions(p)){
                        p.sendMessage("&cYou do not have permission to use this command!");
                        return false;
                    }else
                    LobbyCommands.enabled = false;
                    p.sendMessage(colo(prefix + " &cServer has been shut down."));
                    for(Player ps : Bukkit.getOnlinePlayers()) {
                        if (lobby.getLanguageStorage().useEnglish(ps)) {
                            Title title = new Title(colo("&e&lHiso&f&lCraft"), colo("&cNetwork has been &4restricted"));
                            title.send(p);
                        }else{
                            Title title = new Title(colo("&e&lHiso&f&lCraft"), colo("&cRed a sido &4restrictido"));
                            title.send(p);
                        }
                        ps.playSound(ps.getLocation(), Sound.WITHER_DEATH, 1, 5);
                        if(!ps.isOp()) {
                            ps.teleport(lobby.getLocationHandler().getLocation("Spawn"));
                        }
                        Bukkit.getWorld("Lobby").setTime(16000);
                    }
                    new FireworkRunnable(lobby).runTaskTimer(lobby,5, 5);
                }

                if (args[0].equalsIgnoreCase("unblock")) {
                    if(!CoreAPI.getRanks().hasSuperiorSystemPermissions(p)){
                        p.sendMessage("&cYou do not have permission to use this command!");
                        return false;
                    }else
                    LobbyCommands.enabled = true;
                    p.sendMessage(colo(prefix + " &aServer has been placed online"));
                    for(Player ps : Bukkit.getOnlinePlayers()) {
                        if (lobby.getLanguageStorage().useEnglish(ps)) {
                            Title title = new Title(colo("&e&lHiso&f&lCraft"), colo("&aNetwork has been &2Reactivated"));
                            title.send(p);
                        }else{
                            Title title = new Title(colo("&e&lHiso&f&lCraft"), colo("&aRed a sido &2reactivado"));
                            title.send(p);
                        }
                        ps.playSound(ps.getLocation(), Sound.CAT_PURREOW, 5, 7);
                        if(!ps.isOp()) {
                            ps.teleport(lobby.getLocationHandler().getLocation("Spawn"));
                        }
                        Bukkit.getWorld("Lobby").setTime(5000);
                    }
                    new FireworkRunnable(lobby).runTaskTimer(lobby,5, 5);
                }

                if (args[0].equalsIgnoreCase("english")) {

                    CoreAPI.getStats().setLangauge(p, 1);
                    lobby.getLobbyScoreboards().removePlayer(p);
                    p.sendMessage(colo(prefix + " &bLanguage has been set to English."));
                    try {
                        LoginInventory.giveInventory(p, lobby, false);
                    } catch (SQLException | ClassNotFoundException throwables) {
                        throwables.printStackTrace();
                    }
                        lobby.getLobbyScoreboards().setupScoreboard(p);

                    p.sendMessage(colo(prefix + " &aRelogged."));
                    p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5 );

                    return true;
                }
                if (args[0].equalsIgnoreCase("spanish")) {


                        CoreAPI.getStats().setLangauge(p, 0);
                    lobby.getLobbyScoreboards().removePlayer(p);
                    p.sendMessage(colo(prefix + " &bIdioma ha sido configurado a Espanol."));
                    try {
                        LoginInventory.giveInventory(p, lobby, false);
                    } catch (SQLException | ClassNotFoundException throwables) {
                        throwables.printStackTrace();
                    }
                        lobby.getLobbyScoreboards().setupScoreboard(p);

                    p.sendMessage(colo(prefix + " &aReformado."));
                    p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5 );

                    return true;
                }
            }else{
                p.sendMessage(colo(prefix + " &cYou do not have permission to run this command."));
                return true;
            }
        }

        return false;
    }

    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }
}
