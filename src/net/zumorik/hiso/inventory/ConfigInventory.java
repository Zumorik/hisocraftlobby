package net.zumorik.hiso.inventory;

import net.zumorik.hiso.main.Lobby;
import net.zumorik.hiso.util.ItemCreator;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;

public class ConfigInventory {

    private Lobby lobby;

    public ConfigInventory(Lobby lobby){
        this.lobby = lobby;
    }


    public void openInventory(Player player) throws SQLException, ClassNotFoundException {

        if(lobby.getLanguageStorage().useEnglish(player)){
            Inventory inventory = Bukkit.createInventory(null, 27, colo("&8Settings"));

            inventory.setItem(10, new ItemCreator(new ItemStack(Material.SUGAR)).setName("&bSpeed Boost").lores(" " , "&aDonator Feature", " ").toItemStack());
            inventory.setItem(13, new ItemCreator(new ItemStack(Material.BOOK)).setName("&bSpanish").lores(" ", "&aChange your language", "&ato spanish." , " ").toItemStack());
            inventory.setItem(16, new ItemCreator(new ItemStack(Material.FEATHER)).setName("&bFly").lores(" " , "&aDonator Feature", " ").toItemStack());

            player.openInventory(inventory);
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 10,10);
        }else{
            Inventory inventory = Bukkit.createInventory(null, 27, colo("&8Configuracion"));

            inventory.setItem(10, new ItemCreator(new ItemStack(Material.SUGAR)).setName("&bAumento de Velocidad").lores(" " , "&aFuncion de VIP", " ").toItemStack());
            inventory.setItem(13, new ItemCreator(new ItemStack(Material.BOOK)).setName("&bIngles").lores(" ", "&aCambia tu lenguaje", "&aa ingles." , " ").toItemStack());
            inventory.setItem(16, new ItemCreator(new ItemStack(Material.FEATHER)).setName("&bVolar").lores(" " , "&aFuncion de VIP", " ").toItemStack());

            player.openInventory(inventory);
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 10,10);
        }


    }


    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }
}
