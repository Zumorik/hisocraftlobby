package net.zumorik.hiso.inventory;

import net.zumorik.hiso.main.Lobby;
import net.zumorik.hiso.util.ItemCreator;
import net.zumorik.main.api.CoreAPI;
import net.zumorik.main.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DailyRewardsInventory {

    private Lobby lobby;

    public DailyRewardsInventory(Lobby lobby){
        this.lobby = lobby;
    }


    public void openInventory(Player player){

        if(lobby.getLanguageStorage().useEnglish(player)){
            Inventory inventory = Bukkit.createInventory(null, 54, colo("&8Daily Rewards:"));

            LocalDateTime time = CoreAPI.getCore().getDailyDataCache().getMonthly(player);

            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            String formatDate = time.format(format);

            if(time.getYear() == 2017 || LocalDateTime.now().isAfter(time.plusMonths(1))){
                inventory.setItem(11, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eMonthly Reward").lores(
                        "&7Monthly user reward",
                        "&7This includes:",
                        " &6+1500 Coins",
                        " &b+500 XP", "", "&aClick to Claim").toItemStack());
            }else{
                LocalDateTime news = time.plusMonths(1);
                inventory.setItem(11, new ItemCreator(new ItemStack(Material.COAL)).setName("&eMonthly Reward").lores(
                        "&7Monthly user reward",
                        "&7This includes:",
                        " &6+1500 Coins",
                        " &b+500 XP", "", "&cCLAIMED MUST WAIT UNTIL AFTER " , "&c" +  time.getMonthValue() + "/" + time.getDayOfMonth() + "/" +time.getYear()).toItemStack());
            }


            LocalDateTime timevip = CoreAPI.getCore().getDailyDataCache().getMonthlyVIP(player);
            String formatDatevip = time.format(format);
            boolean canUse = player.hasPermission("award.pvp");
            String vip = canUse ? "&aClick to Claim" : "&cYou do not have Permission";

            if(timevip.getYear() == 2017 || LocalDateTime.now().isAfter(timevip.plusMonths(1))){
                inventory.setItem(12, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eMonthly &a&lVIP &eReward").lores(
                        "&7Monthly vip reward",
                        "&7This includes:",
                        " &6+2500 Coins",
                        " &b+750 XP", "", vip).toItemStack());
            }else{
                LocalDateTime news = time.plusMonths(1);
                inventory.setItem(12, new ItemCreator(new ItemStack(Material.COAL)).setName("&eMonthly &a&lVIP &eReward").lores(
                        "&7Monthly user reward",
                        "&7This includes:",
                        " &6+1500 Coins",
                        " &b+500 XP", "", "&cCLAIMED MUST WAIT UNTIL AFTER " , "&c" +  timevip.getMonthValue() + "/" + timevip.getDayOfMonth() + "/" +timevip.getYear()).toItemStack());
            }

            LocalDateTime timeepic = CoreAPI.getCore().getDailyDataCache().getMonthlyEpic(player);
            String formatDateepic = time.format(format);
            boolean canUseEpic = player.hasPermission("award.epic");
            String epic = canUse ? "&aClick to Claim" : "&cYou do not have Permission";

            if(timeepic.getYear() == 2017 || LocalDateTime.now().isAfter(timeepic.plusMonths(1))){
                inventory.setItem(13, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eMonthly &3&lEPIC &eReward").lores(
                        "&7Monthly epic reward",
                        "&7This includes:",
                        " &6+3500 Coins",
                        " &b+1000 XP", "", epic).toItemStack());
            }else{
                LocalDateTime news = time.plusMonths(1);
                inventory.setItem(13, new ItemCreator(new ItemStack(Material.COAL)).setName("&eMonthly &3&lEPIC &eReward").lores(
                        "&7Monthly user reward",
                        "&7This includes:",
                        " &6+1500 Coins",
                        " &b+500 XP", "", "&cCLAIMED MUST WAIT UNTIL AFTER " , "&c" + timeepic.getMonthValue() + "/" + timeepic.getDayOfMonth() + "/" +timeepic.getYear()).toItemStack());
            }

            LocalDateTime timeelite = CoreAPI.getCore().getDailyDataCache().getMonthlyElite(player);
            String formatDateelite = time.format(format);
            boolean canUseElite = player.hasPermission("award.elite");
            String elite = canUse ? "&aClick to Claim" : "&cYou do not have Permission";

            if(timeelite.getYear() == 2017 || LocalDateTime.now().isAfter(timeelite.plusMonths(1))){
                inventory.setItem(14, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eMonthly &d&lELITE &eReward").lores(
                        "&7Monthly elite reward",
                        "&7This includes:",
                        " &6+4500 Coins",
                        " &b+1250 XP", "", elite).toItemStack());
            }else{
                LocalDateTime news = time.plusMonths(1);
                inventory.setItem(14, new ItemCreator(new ItemStack(Material.COAL)).setName("&eMonthly &d&lELITE &eReward").lores(
                        "&7Monthly user reward",
                        "&7This includes:",
                        " &6+1500 Coins",
                        " &b+500 XP", "", "&cCLAIMED MUST WAIT UNTIL AFTER " , "&c" + timeelite.getMonthValue() + "/" + timeelite.getDayOfMonth() + "/" +timeelite.getYear()).toItemStack());
            }

            LocalDateTime timeultra = CoreAPI.getCore().getDailyDataCache().getMonthlyUltra(player);
            String formatDateultra = time.format(format);
            boolean canUseultra = player.hasPermission("award.ultra");
            String ultra = canUse ? "&aClick to Claim" : "&cYou do not have Permission";

            if(timeultra.getYear() == 2017 || LocalDateTime.now().isAfter(timeultra.plusMonths(1))){
                inventory.setItem(15, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eMonthly &9&lULTRA &eReward").lores(
                        "&7Monthly ultra reward",
                        "&7This includes:",
                        " &6+5500 Coins",
                        " &b+1500 XP", "", ultra).toItemStack());
            }else{
                LocalDateTime news = time.plusMonths(1);
                inventory.setItem(15, new ItemCreator(new ItemStack(Material.COAL)).setName("&eMonthly &9&lULTRA &eReward").lores(
                        "&7Monthly user reward",
                        "&7This includes:",
                        " &6+1500 Coins",
                        " &b+500 XP", "", "&cCLAIMED MUST WAIT UNTIL AFTER " , "&c" + timeultra.getMonthValue() + "/" + timeultra.getDayOfMonth() + "/" +timeultra.getYear()).toItemStack());
            }

            if(CoreAPI.getCore().getDailyDataCache().getFirstTime(player) == 0){
                inventory.setItem(30, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eFirst Time").lores(
                        "&7First time user reward",
                        "&7This includes:",
                        " &6+1500 Coins",
                        " &b+1000 XP", "", ultra).toItemStack());
            }

            LocalDateTime timedaily = CoreAPI.getCore().getDailyDataCache().getDaily(player);
            String formatDatedaily = time.format(format);
            String daily = LocalDateTime.now().isAfter(timedaily) ?  "&aClick to Claim" : "&cYou must wait until after &9" + formatDatedaily;



            if(timedaily.getYear() == 2017 || LocalDateTime.now().isAfter(timedaily)){
                inventory.setItem(31, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eDaily Reward").lores(
                        "&7Daily user reward",
                        "&7This includes:",
                        " &6+1000 Coins",
                        " &b+150 XP", "", daily).toItemStack());
            }

            if(CoreAPI.getCore().getDailyDataCache().getSecondTime(player) == 0){
                inventory.setItem(32, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eSecond Time").lores(
                        "&7Second time user reward",
                        "&7This includes:",
                        " &6+500 Coins",
                        " &b+100 XP", "", ultra).toItemStack());
            }

                inventory.setItem(38, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eVote #1").lores(
                        "&7Vote reward",
                        "&7This includes:",
                        " &6+500 Coins",
                        " &b+100 XP", "", "&aClick to Claim!").toItemStack());
            inventory.setItem(39, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eVote #2").lores(
                    "&7Vote reward",
                    "&7This includes:",
                    " &6+500 Coins",
                    " &b+100 XP", "", "&aClick to Claim!").toItemStack());

            ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            SkullMeta meta = (SkullMeta) skull.getItemMeta();
            meta.setOwner("Dipicrylamine");
            meta.setDisplayName(colo("&aAd Reward"));
            skull.setItemMeta(meta);

            inventory.setItem(40, skull);

            inventory.setItem(41, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eVote #3").lores(
                    "&7Vote reward",
                    "&7This includes:",
                    " &6+500 Coins",
                    " &b+100 XP", "", "&aClick to Claim!").toItemStack());
            inventory.setItem(42, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eVote #4").lores(
                    "&7Vote reward",
                    "&7This includes:",
                    " &6+500 Coins",
                    " &b+100 XP", "", "&aClick to Claim!").toItemStack());


            player.openInventory(inventory);
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 10,10);
        }else{
            Inventory inventory = Bukkit.createInventory(null, 54, colo("&8Recompensas diarias:"));

            LocalDateTime time = CoreAPI.getCore().getDailyDataCache().getMonthly(player);

            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            String formatDate = time.format(format);

            if(time.getYear() == 2017 || time.plusMonths(1).isAfter(LocalDateTime.now())){
                inventory.setItem(11, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eRecompensa Mensual").lores(
                        "&7Recompensa mensual de usario",
                        "&7Esto incluye:",
                        " &6+1500 Gemas",
                        " &b+500 XP", "", "&aHaga clic para reclamar").toItemStack());
            }


            LocalDateTime timevip = CoreAPI.getCore().getDailyDataCache().getMonthlyVIP(player);
            String formatDatevip = time.format(format);
            boolean canUse = player.hasPermission("award.pvp");
            String vip = canUse ? "&aHaga clic para reclamar" : "&cNo tienes Permiso";

            if(timevip.getYear() == 2017 || timevip.plusMonths(1).isAfter(LocalDateTime.now())){
                inventory.setItem(12, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eRecompensa Mensual de &a&lVIP").lores(
                        "&7Recompensa mensual de vip",
                        "&7Esto incluye:",
                        " &6+2500 Gemas",
                        " &b+750 XP", "", vip).toItemStack());
            }

            LocalDateTime timeepic = CoreAPI.getCore().getDailyDataCache().getMonthlyEpic(player);
            String formatDateepic = time.format(format);
            boolean canUseEpic = player.hasPermission("award.epic");
            String epic = canUseEpic ? "&aHaga clic para reclamar" : "&cNo tienes Permiso";

            if(timeepic.getYear() == 2017 || timeepic.plusMonths(1).isAfter(LocalDateTime.now())){
                inventory.setItem(13, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eRecompensa Mensual de &3&lEPIC").lores(
                        "&7Recompensa mensual de epic",
                        "&7Esto incluye:",
                        " &6+3500 Gemas",
                        " &b+1000 XP", "", epic).toItemStack());
            }

            LocalDateTime timeelite = CoreAPI.getCore().getDailyDataCache().getMonthlyElite(player);
            String formatDateelite = time.format(format);
            boolean canUseElite = player.hasPermission("award.elite");
            String elite = canUse ? "&aClick to Claim" : "&cYou do not have Permission";

            if(timeelite.getYear() == 2017 || timeelite.plusMonths(1).isAfter(LocalDateTime.now())){
                inventory.setItem(14, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eRecompensa Mensual de &d&lELITE").lores(
                        "&7Recompensa mensual de elite",
                        "&7Esto incluye:",
                        " &6+4500 Gemas",
                        " &b+1250 XP", "", elite).toItemStack());
            }

            LocalDateTime timeultra = CoreAPI.getCore().getDailyDataCache().getMonthlyUltra(player);
            String formatDateultra = time.format(format);
            boolean canUseultra = player.hasPermission("award.ultra");
            String ultra = canUse ? "&aHaga clic para reclamar" : "&cNo tienes Permiso";

            if(timeultra.getYear() == 2017 || timeultra.plusMonths(1).isAfter(LocalDateTime.now())){
                inventory.setItem(15, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eRecompensa Mensual de &9&lULTRA").lores(
                        "&7Recompensa mensual de ultra",
                        "&7Esto incluye:",
                        " &6+5500 Gemas",
                        " &b+1500 XP", "", ultra).toItemStack());
            }

            if(CoreAPI.getCore().getDailyDataCache().getFirstTime(player) == 0){
                inventory.setItem(30, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&ePrimera Vez").lores(
                        "&7Recompensa de un uso",
                        "&7Esto incluye:",
                        " &6+1500 Gemas",
                        " &b+1000 XP", "", ultra).toItemStack());
            }

            LocalDateTime timedaily = CoreAPI.getCore().getDailyDataCache().getDaily(player);
            String formatDatedaily = time.format(format);
            String daily = LocalDateTime.now().isAfter(timedaily) ?  "&ahaga clic para reclamar" : "&cTienes que esperar hasta después de &9" + formatDatedaily;



            if(timedaily.getYear() == 2017 || LocalDateTime.now().isAfter(timedaily)){
                inventory.setItem(31, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eRecompensa Diaria").lores(
                        "&7Recompensa de usario diario",
                        "&7Esto incluye:",
                        " &6+1000 Gemas",
                        " &b+150 XP", "", daily).toItemStack());
            }

            if(CoreAPI.getCore().getDailyDataCache().getSecondTime(player) == 0){
                inventory.setItem(32, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eSegunda Vez").lores(
                        "&7Recompensa de un uso",
                        "&7Esto incluye:",
                        " &6+500 Gemas",
                        " &b+100 XP", "", ultra).toItemStack());
            }

            inventory.setItem(38, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eVotar #1").lores(
                    "&7Recompensa de votar",
                    "&7Esto incluye:",
                    " &6+500 Gemas",
                    " &b+100 XP", "", "&aHaga clic para reclamar").toItemStack());
            inventory.setItem(39, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eVotar #2").lores(
                    "&7Recompensa de votar",
                    "&7Esto incluye:",
                    " &6+500 Gemas",
                    " &b+100 XP", "", "&aHaga clic para reclamar").toItemStack());

            ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            SkullMeta meta = (SkullMeta) skull.getItemMeta();
            meta.setOwner("Dipicrylamine");
            meta.setDisplayName(colo("&aRecompensa de anuncio"));
            skull.setItemMeta(meta);

            inventory.setItem(40, skull);

            inventory.setItem(41, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eVotar #3").lores(
                    "&7Recompensa de votar",
                    "&7Esto incluye:",
                    " &6+500 Gemas",
                    " &b+100 XP", "", "&aHaga clic para reclamar").toItemStack());
            inventory.setItem(42, new ItemCreator(new ItemStack(Material.STORAGE_MINECART)).setName("&eVotar #4").lores(
                    "&7Recompensa de votar",
                    "&7Esto incluye:",
                    " &6+500 Gemas",
                    " &b+100 XP", "", "&aHaga clic para reclamar").toItemStack());


            player.openInventory(inventory);
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 10,10);
        }
    }


    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }
}
