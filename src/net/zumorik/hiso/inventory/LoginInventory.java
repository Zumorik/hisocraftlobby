package net.zumorik.hiso.inventory;

import net.zumorik.hiso.main.Lobby;
import net.zumorik.hiso.util.ItemCreator;
import net.zumorik.hiso.util.Title;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.sql.SQLException;

public class LoginInventory {

public static void giveInventory(Player p, Lobby lobby, boolean play) throws SQLException, ClassNotFoundException {
        if(CoreAPI.getStats().getLanguage(p) == 1){
            p.getInventory().setItem(0, new ItemCreator(new ItemStack(Material.COMPASS)).setName("&e&lSERVERS").toItemStack());
            p.getInventory().setItem(4, new ItemCreator(new ItemStack(Material.EYE_OF_ENDER)).setName("&e&lSTORE").toItemStack());


            ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            SkullMeta meta = (SkullMeta) skull.getItemMeta();
            meta.setOwner(p.getName());
            meta.setDisplayName(colo("&e&lSETTINGS"));
            skull.setItemMeta(meta);

            p.getInventory().setItem(8, skull);
            if(play) {
                Title titles = new Title(colo("&e&lHiso&f&lCraft"), colo("&aWelcome to HisoCraft!"));
                titles.send(p);
            }
        }else {
            p.getInventory().setItem(0, new ItemCreator(new ItemStack(Material.COMPASS)).setName("&e&lSERVIDORES").toItemStack());
            p.getInventory().setItem(4, new ItemCreator(new ItemStack(Material.EYE_OF_ENDER)).setName("&e&lTIENDA").toItemStack());

            ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            SkullMeta meta = (SkullMeta) skull.getItemMeta();
            meta.setOwner(p.getName());
            meta.setDisplayName(colo("&e&lCONFIGURACION"));
            skull.setItemMeta(meta);

            p.getInventory().setItem(8, skull);
            if(play) {
                Title title = new Title(colo("&e&lHiso&f&lCraft"), colo("&aBien Venido a HisoCraft!"));
                title.send(p);
            }
        }
}

public static String colo(String s){
    return ChatColor.translateAlternateColorCodes('&',s);
}

}
