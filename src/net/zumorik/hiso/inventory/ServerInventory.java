package net.zumorik.hiso.inventory;

import net.zumorik.hiso.main.Lobby;
import net.zumorik.hiso.util.ItemCreator;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ServerInventory {

    private Lobby lobby;

    public ServerInventory(Lobby lobby){
        this.lobby = lobby;
    }


    public void openInventory(Player player){

        if(lobby.getLanguageStorage().useEnglish(player)){
            Inventory inventory = Bukkit.createInventory(null, 54, colo("&8What do you want to play?"));

            inventory.setItem(12, new ItemCreator(new ItemStack(Material.BOW)).setName(colo("&e&lNEW! &aSkyWars &c&lBETA")).lores(
                    " "," ","&7Battle on a island with",
                    "&7others last player standing",
                    "&7wins the game!", " ", "&7There are &aNULL &7players online!" ).toItemStack());
            inventory.setItem(13, new ItemCreator(new ItemStack(Material.IRON_SWORD)).setName(colo("&e&lNEW! &aPracticePvP &c&lBETA")).lores(
                    " "," ","&7Competative PvP with kits",
                    "&7either duels or solo" , " ", "&7There are &aNULL &7players online!").toItemStack());
            inventory.setItem(14, new ItemCreator(new ItemStack(Material.BED)).setName(colo("&e&lNEW! &aBedWars &c&lBETA")).lores(
                    " "," ","&7Fight against other teams",
                    "&7collect minerals to advance",
                    "&7your team, destroy beds to",
                    "&7eliminate teams!", ""
                    ,"&7There are &aNULL &7players online!").toItemStack());

            inventory.setItem(20, new ItemCreator(new ItemStack(Material.GRASS)).setName(colo("&c&lSOON! &aBedWars")).toItemStack());
            inventory.setItem(21, new ItemCreator(new ItemStack(Material.WORKBENCH)).setName(colo("&c&lSOON! &aCeative")).toItemStack());
            inventory.setItem(22, new ItemCreator(new ItemStack(Material.APPLE)).setName(colo("&c&lSOON! &aSurvival Games")).toItemStack());
            inventory.setItem(23, new ItemCreator(new ItemStack(Material.IRON_HELMET)).setName(colo("&c&lSOON! &aSurvival")).toItemStack());
            inventory.setItem(24, new ItemCreator(new ItemStack(Material.IRON_AXE)).setName(colo("&c&lSOON! &aThe Pit")).toItemStack());

            inventory.setItem(47, new ItemCreator(new ItemStack(Material.BOOKSHELF)).setName(colo("&aLobby Selector")).toItemStack());
            inventory.setItem(49, new ItemCreator(new ItemStack(Material.BOOK)).setName(colo("&bServer Info")).lores(" ", "&7", " ").toItemStack());
            inventory.setItem(51, new ItemCreator(new ItemStack(Material.REDSTONE)).setName(colo("&cCLOSE")).toItemStack());

            player.openInventory(inventory);
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 10,10);
        }else{
            Inventory inventory = Bukkit.createInventory(null, 54, colo("&8Que quieres jugar?"));


            inventory.setItem(12, new ItemCreator(new ItemStack(Material.BOW)).setName(colo("&e&lNEW! &aSkyWars &c&lBETA")).lores(
                    " "," ","&7Sobrevive hasta el final de",
                    "&7la partida asesinando a los",
                    "&7jugadores de las demas islas.", " ", "&7Hay &aNULL &7usuarios jugando!" ).toItemStack());
            inventory.setItem(13, new ItemCreator(new ItemStack(Material.IRON_SWORD)).setName(colo("&e&lNEW! &aPracticePvP &c&lBETA")).lores(
                    " "," ","&7PvP Competativa con kits ",
                    "&7duels o solo" , " ", "&7Hay &aNULL &7usuarios jugando!").toItemStack());
            inventory.setItem(14, new ItemCreator(new ItemStack(Material.BED)).setName(colo("&e&lNEW! &aBedWars &c&lBETA")).lores(
                    " "," ",
                    "&7Destrure la cama de tu enemigo",
                    "&7y recolecta minerales para","&7hacerte con la victoria!", "" ,"&7Hay &aNULL &7usuarios jugando!").toItemStack());


            inventory.setItem(20, new ItemCreator(new ItemStack(Material.GRASS)).setName(colo("&c&lPROXIMAMENTE! &aBedWars")).toItemStack());
            inventory.setItem(21, new ItemCreator(new ItemStack(Material.WORKBENCH)).setName(colo("&c&lPROXIMAMENTE! &aCeative")).toItemStack());
            inventory.setItem(22, new ItemCreator(new ItemStack(Material.APPLE)).setName(colo("&c&lPROXIMAMENTE! &aSurvival Games")).toItemStack());
            inventory.setItem(23, new ItemCreator(new ItemStack(Material.IRON_HELMET)).setName(colo("&c&lPROXIMAMENTE! &aSurvival")).toItemStack());
            inventory.setItem(24, new ItemCreator(new ItemStack(Material.IRON_AXE)).setName(colo("&c&lPROXIMAMENTE! &aEl Pit")).toItemStack());

            inventory.setItem(47, new ItemCreator(new ItemStack(Material.BOOKSHELF)).setName(colo("&aSelector de Lobby")).toItemStack());
            inventory.setItem(49, new ItemCreator(new ItemStack(Material.BOOK)).setName(colo("&bInfo de Server")).lores(" ", "&7", " ").toItemStack());
            inventory.setItem(51, new ItemCreator(new ItemStack(Material.REDSTONE)).setName(colo("&cSalir")).toItemStack());



            player.openInventory(inventory);
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 10,10);
        }


    }


    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }
}
