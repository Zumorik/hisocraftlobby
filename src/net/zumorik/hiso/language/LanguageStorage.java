package net.zumorik.hiso.language;

import net.zumorik.hiso.main.Lobby;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.ArrayList;

public class LanguageStorage {

    private Lobby lobby;

    public ArrayList<String> english;

    public LanguageStorage(Lobby lobby){
        this.lobby = lobby;
        this.english = new ArrayList<String>();
    }

    public boolean useEnglish(Player p) {

        boolean use = false;
            if(CoreAPI.getStats().getLanguage(p) == 1) {
                use = true;
                return true;
            }
            else
                use = false;
        return use;
    }



    public ArrayList<String> getEnglish() {
        return lobby.getLanguageStorage().english;
    }
}
