package net.zumorik.hiso.listeners;

import net.minecraft.server.v1_8_R3.EntityArrow;
import net.zumorik.hiso.inventory.ConfigInventory;
import net.zumorik.hiso.inventory.LoginInventory;
import net.zumorik.hiso.main.Lobby;
import net.zumorik.hiso.util.ItemCreator;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DailyRewardInventoryListener implements Listener {

    private Lobby lobby;

    public DailyRewardInventoryListener(Lobby lobby) {
        this.lobby = lobby;
    }

    private String prefix = "&e&lHiso&f&lCraft &7:";

    @EventHandler
    public void inventoryInteract(InventoryClickEvent e) throws SQLException, ClassNotFoundException {
        Player p = (Player) e.getWhoClicked();
        if (lobby.getRegisterListeners().getBlocks().getBuilders().contains(p)) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }

        if (e.getClickedInventory().getName().contains("Recompensas diarias") || e.getClickedInventory().getName().contains("Daily Rewards:")) {
            if (e.getCurrentItem() != null && e.getClickedInventory() != null) {
                if (e.getCurrentItem().getItemMeta() != null) {

                    ItemStack item = e.getCurrentItem();

                    if(item.getItemMeta().getDisplayName().contains("Monthly Reward")){
                        LocalDateTime time = CoreAPI.getCore().getDailyDataCache().getMonthly(p);

                        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                        String formatDate = time.format(format);

                        if(lobby.getLanguageStorage().useEnglish(p)){

                            if(time.getYear() == 2017 || LocalDateTime.now().isAfter(time.plusMonths(1))){

                                CoreAPI.getCore().getDailyDataCache().setUser(p, LocalDateTime.now().format(format).toString());

                                CoreAPI.getStats().addCoins(p,1500);
                                CoreAPI.getStats().addXP(p, 500);

                                p.closeInventory();

                                p.sendMessage(colo(prefix + " &6+1500 &aCoins"));
                                p.sendMessage(colo(prefix + " &b+500 &aXP"));
                                p.sendMessage(" ");
                                p.sendMessage(colo(prefix + " &eMonthly award has been claimed!"));

                            }

                        }else{
                            //spanish
                        }

                    }
                    if(item.getItemMeta().getDisplayName().contains("VIP")){
                        LocalDateTime time = CoreAPI.getCore().getDailyDataCache().getMonthlyVIP(p);

                        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                        String formatDate = time.format(format);

                        if(lobby.getLanguageStorage().useEnglish(p)){

                            if(time.getYear() == 2017 || LocalDateTime.now().isAfter(time.plusMonths(1))){

                                CoreAPI.getCore().getDailyDataCache().setVIP(p, LocalDateTime.now().format(format).toString());

                                CoreAPI.getStats().addCoins(p,2500);
                                CoreAPI.getStats().addXP(p, 750);

                                p.closeInventory();

                                p.sendMessage(colo(prefix + " &6+2500 &aCoins"));
                                p.sendMessage(colo(prefix + " &b+750 &aXP"));
                                p.sendMessage(" ");
                                p.sendMessage(colo(prefix + " &eMonthly award has been claimed!"));

                            }

                        }else{
                            //spanish
                        }
                    }
                    if(item.getItemMeta().getDisplayName().contains("EPIC")){
                        LocalDateTime time = CoreAPI.getCore().getDailyDataCache().getMonthlyEpic(p);

                        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                        String formatDate = time.format(format);

                        if(lobby.getLanguageStorage().useEnglish(p)){

                            if(time.getYear() == 2017 || LocalDateTime.now().isAfter(time.plusMonths(1))){

                                CoreAPI.getCore().getDailyDataCache().setEpic(p, LocalDateTime.now().format(format).toString());

                                CoreAPI.getStats().addCoins(p,2500);
                                CoreAPI.getStats().addXP(p, 750);

                                p.closeInventory();

                                p.sendMessage(colo(prefix + " &6+3500 &aCoins"));
                                p.sendMessage(colo(prefix + " &b+1000 &aXP"));
                                p.sendMessage(" ");
                                p.sendMessage(colo(prefix + " &eMonthly award has been claimed!"));

                            }

                        }else{
                            //spanish
                        }
                    }
                    if(item.getItemMeta().getDisplayName().contains("ELITE")){
                        LocalDateTime time = CoreAPI.getCore().getDailyDataCache().getMonthlyElite(p);

                        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                        String formatDate = time.format(format);

                        if(lobby.getLanguageStorage().useEnglish(p)){

                            if(time.getYear() == 2017 || LocalDateTime.now().isAfter(time.plusMonths(1))){

                                CoreAPI.getCore().getDailyDataCache().setElite(p, LocalDateTime.now().format(format).toString());

                                CoreAPI.getStats().addCoins(p,4500);
                                CoreAPI.getStats().addXP(p, 1250);

                                p.closeInventory();

                                p.sendMessage(colo(prefix + " &6+4500 &aCoins"));
                                p.sendMessage(colo(prefix + " &b+1250 &aXP"));
                                p.sendMessage(" ");
                                p.sendMessage(colo(prefix + " &eMonthly award has been claimed!"));

                            }

                        }else{
                            //spanish
                        }
                    }
                    if(item.getItemMeta().getDisplayName().contains("ULTRA")){
                        LocalDateTime time = CoreAPI.getCore().getDailyDataCache().getMonthlyUltra(p);

                        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                        String formatDate = time.format(format);

                        if(lobby.getLanguageStorage().useEnglish(p)){

                            if(time.getYear() == 2017 || LocalDateTime.now().isAfter(time.plusMonths(1))){

                                CoreAPI.getCore().getDailyDataCache().setUltra(p, LocalDateTime.now().format(format).toString());

                                CoreAPI.getStats().addCoins(p,5500);
                                CoreAPI.getStats().addXP(p, 1500);

                                p.closeInventory();

                                p.sendMessage(colo(prefix + " &6+5500 &aCoins"));
                                p.sendMessage(colo(prefix + " &b+1500 &aXP"));
                                p.sendMessage(" ");
                                p.sendMessage(colo(prefix + " &eMonthly award has been claimed!"));

                            }

                        }else{
                            //spanish
                        }
                    }

                }
            }
        }
    }

    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }
}