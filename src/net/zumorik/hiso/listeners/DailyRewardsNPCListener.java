package net.zumorik.hiso.listeners;

import net.citizensnpcs.api.event.NPCClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.zumorik.hiso.inventory.DailyRewardsInventory;
import net.zumorik.hiso.main.Lobby;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class DailyRewardsNPCListener implements Listener {

    private Lobby lobby;

    public DailyRewardsNPCListener(Lobby lobby){
        this.lobby = lobby;
    }


    @EventHandler
    public void onclick(NPCRightClickEvent e){
            DailyRewardsInventory inv = new DailyRewardsInventory(lobby);
           inv.openInventory(e.getClicker());
    }


    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }


}
