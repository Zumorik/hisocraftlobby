package net.zumorik.hiso.listeners;

import net.minecraft.server.v1_8_R3.EntityItem;
import net.minecraft.server.v1_8_R3.ItemEnchantedBook;
import net.zumorik.hiso.main.Lobby;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftItem;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ParkourTest implements Listener {

    private Lobby lobby;
    private int max = 0;

    private Location loc;

    public ParkourTest(Lobby lobby){
        this.lobby = lobby;
    }

    public void play(Player p){
        Location loc = p.getLocation();
        Bukkit.getScheduler().scheduleSyncRepeatingTask(lobby, () ->{
            if(max >= 30){
                return;
            }
            generateBlock(loc);
            max++;
        },0,40);
    }

    public void generateBlock(Location loc){
        int x = getRandomInteger(3,-1);
        int y = getRandomInteger(1,0);
        int z = getRandomInteger(3,-1);

        int pos1 = getRandomInteger(3,0);
        int pos2 = getRandomInteger(3,0);

        int id = getRandomInteger(15,0);

        if(pos1 == 1 || pos1 == 2){
            x = (x)*(-1);
        }


        if(pos2 == 2 || pos1 == 3){
            z = (z)*(-1);
        }

        Location newLoc = loc.clone().add(x,y,z);

        List<Block> remove = new ArrayList<Block>();
        List<Item> removes = new ArrayList<Item>();
        if(newLoc.getBlock().getType().equals(Material.AIR)){
            if(!(loc == newLoc)) {
                newLoc.getBlock().setType(Material.STAINED_CLAY);
                newLoc.getBlock().setData((byte) id);
                newLoc.getWorld().playEffect(newLoc, Effect.STEP_SOUND, 95);
                if(pos1 == 2) {
                    Item s = newLoc.getWorld().dropItem(newLoc.clone().add(0, 2, 0), new ItemStack(Material.EMERALD));
                    s.setVelocity(new Vector());
                    removes.add(s);

                }
                remove.add(newLoc.getBlock());
                Bukkit.getScheduler().scheduleSyncDelayedTask(lobby, () -> {
                    remove.get(0).setType(Material.AIR);
                    remove.remove(0);
                    if(removes.size() > 0) {
                        removes.get(0).remove();
                        removes.remove(0);
                    }
                }, 80);
            }else{
                generateBlock(newLoc);
            }
        }else{
            generateBlock(newLoc);
        }
    }

    private String colo(String s){
        return  ChatColor.translateAlternateColorCodes('&',s);
    }


    public static int getRandomInteger(int maximum, int minimum){ return ((int) (Math.random()*(maximum - minimum))) + minimum; }
}
