package net.zumorik.hiso.listeners;

import net.zumorik.hiso.main.Lobby;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class PlayerBlockListener implements Listener {

    private Lobby lobby;

    private Set<Player> builders;

    public PlayerBlockListener(Lobby lobby){
        this.lobby = lobby;
        setBuilders(new HashSet<Player>());

    }

    @EventHandler
    public void onPickUp(PlayerPickupItemEvent e) throws SQLException, ClassNotFoundException {
        if(e.getItem().getItemStack().getType() == Material.EMERALD){
            e.getItem().remove();
            e.setCancelled(true);
            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.NOTE_PLING,5,5);
            e.getPlayer().sendMessage(colo("&a+1 &bCoin!"));
            CoreAPI.getEconomy().addCoins(e.getPlayer(),1);
            lobby.getLobbyScoreboards().updateCoins(e.getPlayer());
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e){
        Player p = e.getPlayer();
        if(getBuilders().contains(p)){
            e.setCancelled(false);
        }else{
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockRemove(BlockBreakEvent e){
        Player p = e.getPlayer();
        if(getBuilders().contains(p)){
            e.setCancelled(false);
        }else{
            e.setCancelled(true);
        }
    }
    private String colo(String s){
        return  ChatColor.translateAlternateColorCodes('&',s);
    }

    public Set<Player> getBuilders() {
        return builders;
    }

    public void setBuilders(Set<Player> builders) {
        this.builders = builders;
    }
}
