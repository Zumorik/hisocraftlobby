package net.zumorik.hiso.listeners;

import net.minecraft.server.v1_8_R3.EntityArrow;
import net.zumorik.hiso.inventory.ConfigInventory;
import net.zumorik.hiso.inventory.LoginInventory;
import net.zumorik.hiso.main.Lobby;
import net.zumorik.hiso.util.ItemCreator;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.lang.reflect.Field;
import java.sql.SQLException;

public class PlayerInventoryListener implements Listener {

    private Lobby lobby;
    public PlayerInventoryListener(Lobby lobby){
        this.lobby = lobby;
    }
    private String prefix = "&e&lHiso&f&lCraft &7:";

    @EventHandler
    public void inventoryInteract(InventoryClickEvent e) throws SQLException, ClassNotFoundException {
        Player p = (Player) e.getWhoClicked();
        if(lobby.getRegisterListeners().getBlocks().getBuilders().contains(p)){
            e.setCancelled(false);
        }else {
            e.setCancelled(true);
        }

        if(e.getClickedInventory().getName().contains("Settings") || e.getClickedInventory().getName().contains("Configuracion")){
            if(e.getCurrentItem() != null && e.getClickedInventory() != null) {

                if(e.getCurrentItem().getItemMeta() != null)

                if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Speed Boost")
                || e.getCurrentItem().getItemMeta().getDisplayName().contains("Velocidad")) {

                    if(!CoreAPI.getRanks().hasFlyPermissions(p)){
                        if(lobby.getLanguageStorage().useEnglish(p)){
                            p.sendMessage(colo(prefix + " &cYou must be a donator to use this feature!"));
                            p.closeInventory();
                        }else{
                            p.sendMessage(colo(prefix + " &cTienes que ser donador para usar la funcion!"));
                            p.closeInventory();
                        }
                        return;
                    }

                        int val = CoreAPI.getMystery().getSpeed(p);
                        if(val == 0){
                            p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));
                            if(lobby.getLanguageStorage().useEnglish(p)) {
                                p.sendMessage(colo(prefix
                                         + " &bSpeed boost has been enabled!"));
                            }else{
                                p.sendMessage(colo(prefix
                                        + " &bVelocidad a sido activado! "));
                            }
                            p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5);
                            CoreAPI.getMystery().setSpeed(p, 1);
                            p.closeInventory();
                        }else{
                            p.removePotionEffect(PotionEffectType.SPEED);
                            if(lobby.getLanguageStorage().useEnglish(p)) {
                                p.sendMessage(colo(prefix
                                        + " &bSpeed boost has been disabled!"));
                            }else{
                                p.sendMessage(colo(prefix
                                        + " &bVelocidad a sido desactivado! "));
                            }
                            p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5);
                            CoreAPI.getMystery().setSpeed(p, 0);
                            p.closeInventory();
                        }
                }else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Spanish") ||
                        e.getCurrentItem().getItemMeta().getDisplayName().contains("Ingles")) {
                    if(lobby.getLanguageStorage().useEnglish(p)){
                        p.sendMessage(colo(prefix
                                + " &bLenguaje cambiado a espanol!"));
                        p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5);
                        CoreAPI.getStats().setLangauge(p, 0);
                        lobby.getLobbyScoreboards().removePlayer(p);
                        try {
                            LoginInventory.giveInventory(p, lobby, false);
                        } catch (SQLException | ClassNotFoundException throwables) {
                            throwables.printStackTrace();
                        }
                        lobby.getLobbyScoreboards().setupScoreboard(p);
                        p.closeInventory();
                    }else{
                        p.sendMessage(colo(prefix
                                +" &bLanguage has been changed to english!"));
                        p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5);
                        lobby.getLobbyScoreboards().removePlayer(p);
                        CoreAPI.getStats().setLangauge(p, 1);
                        try {
                            LoginInventory.giveInventory(p, lobby, false);
                        } catch (SQLException | ClassNotFoundException throwables) {
                            throwables.printStackTrace();
                        }
                        p.closeInventory();
                        lobby.getLobbyScoreboards().setupScoreboard(p);
                    }
                }else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Fly") ||
                        e.getCurrentItem().getItemMeta().getDisplayName().contains("Volar")) {

                    if(!CoreAPI.getRanks().hasFlyPermissions(p)){
                        if(lobby.getLanguageStorage().useEnglish(p)){
                            p.sendMessage(colo(prefix + " &cYou must be a donator to use this feature!"));
                            p.closeInventory();
                        }else{
                            p.sendMessage(colo(prefix + " &cTienes que ser donador para usar la funcion!"));
                            p.closeInventory();
                        }
                        return;
                    }

                    int val = CoreAPI.getMystery().getFlight(p);
                    if(val == 1){
                        CoreAPI.getMystery().setFlight(p, 0);

                        if(lobby.getLanguageStorage().useEnglish(p)){
                            p.sendMessage(colo(prefix
                                    +" &bFlight has been disabled!"));
                            p.setFlying(false);
                            p.setAllowFlight(false);
                            p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5);
                        }else{
                            p.sendMessage(colo(prefix
                                    +" &bVolar a sido desactivado!"));
                            p.setFlying(false);
                            p.setAllowFlight(false);
                            p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5);
                        }
                        p.closeInventory();

                    }else{
                        CoreAPI.getMystery().setFlight(p, 1);
                        if(lobby.getLanguageStorage().useEnglish(p)){
                            p.sendMessage(colo(prefix
                                    +" &bFlight has been enabled!"));
                            p.setAllowFlight(true);
                            p.setFlying(true);
                            p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5);
                        }else{
                            p.sendMessage(colo(prefix
                                    +" &bVolar a sido activado!"));
                            p.setAllowFlight(true);
                            p.setFlying(true);
                            p.playSound(p.getLocation(), Sound.NOTE_PLING, 5,5);
                        }
                        p.closeInventory();
                    }
                }
            }
        }

    }

    @EventHandler
    public void InteractEvent(PlayerInteractEvent e) throws SQLException, ClassNotFoundException {

        Player p = e.getPlayer();
        if(e.getItem() != null) {
            if (e.getItem().getType().equals(Material.EYE_OF_ENDER)) {
                if(lobby.getLanguageStorage().useEnglish(p)){
                    p.sendMessage(colo(prefix + " &aVisit our store! "));
                    p.sendMessage(colo(prefix + " &ehttp://www.hisocraft.com/page3"));
                }else{
                    p.sendMessage(colo(prefix + " &aVisita nuestra tienda! "));
                    p.sendMessage(colo(prefix + " &ehttp://www.hisocraft.com/page3"));
                }
                e.setCancelled(true);
            }

            if (e.getItem().getType().equals(Material.COMPASS)) {
                lobby.getServerInventory().openInventory(p);
            }

            if (e.getItem().getType().equals(Material.SKULL_ITEM)) {
                ConfigInventory inv = new ConfigInventory(lobby);
                inv.openInventory(p);
            }
        }

    }
    public void onDegrade(PlayerItemDamageEvent e){
        e.setCancelled(true);
    }

private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
}

    @EventHandler
    public void dropItem(PlayerDropItemEvent e){

        Player p = e.getPlayer();

        if(lobby.getRegisterListeners().getBlocks().getBuilders().contains(p)){
            e.setCancelled(false);
        }else {
            e.setCancelled(true);
        }
    }
}
