package net.zumorik.hiso.listeners;

import net.zumorik.hiso.inventory.LoginInventory;
import net.zumorik.hiso.main.Lobby;
import net.zumorik.hiso.util.ItemCreator;
import net.zumorik.hiso.util.Title;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.sql.SQLException;
import java.util.HashMap;

public class PlayerJoinListener implements Listener {

    private Lobby lobby;

    public PlayerJoinListener(Lobby lobby){
        this.lobby = lobby;
    }

    private HashMap<String, Integer> speed = new HashMap<String, Integer>();
    private HashMap<String, Integer> flight = new HashMap<String, Integer>();
    private HashMap<String, Integer> playerVisibility = new HashMap<String, Integer>();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) throws SQLException, ClassNotFoundException {

        e.setJoinMessage(null);
        Player p = e.getPlayer();
        p.getInventory().clear();
        p.teleport(lobby.getLocationHandler().getLocation("Spawn"));
        p.setGameMode(GameMode.ADVENTURE);
        p.setLevel(0);
        p.playSound(p.getLocation(), Sound.NOTE_PLING, 10, 10);

        Bukkit.getScheduler().scheduleSyncDelayedTask(lobby, () ->{
            try {
                LoginInventory.giveInventory(p,lobby, true);
            } catch (SQLException | ClassNotFoundException throwables) {
                throwables.printStackTrace();
            }
            lobby.getLobbyScoreboards().setupScoreboard(p);
            lobby.getLobbyScoreboards().uploadOnline();
        },10);


        speed.put(p.getName(), 0);
        playerVisibility.put(p.getName(), 0);
        flight.put(p.getName(), 0);

        Bukkit.getScheduler().runTaskAsynchronously(lobby, () ->{
                    playerVisibility.put(p.getName(), CoreAPI.getMystery().getPlayer(p));
                    speed.put(p.getName(), CoreAPI.getMystery().getSpeed(p));
                    flight.put(p.getName(), CoreAPI.getMystery().getFlight(p));
        });

        Bukkit.getScheduler().scheduleSyncDelayedTask(lobby, () -> {
            if (p.isOnline()) {
            int visibility = playerVisibility.get(p.getName());
            int speed1 = speed.get(p.getName());
            int flight1 = flight.get(p.getName());
            updateFlight(p, flight1);
            updateSpeed(p, speed1);
        }
        }, 10);

    }

    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }


    public void updateFlight(Player p, int setting) {
        switch (setting) {
            case 0:
                p.setAllowFlight(false);
                p.setFlying(false);
                break;
            case 1:
                p.setAllowFlight(true);
                break;
        }
    }

    public void updateSpeed(Player p, int setting) {
        switch (setting) {
            case 0:
                p.removePotionEffect(PotionEffectType.SPEED);
                break;
            case 1:
                p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2, false));
                break;
        }
    }

}
