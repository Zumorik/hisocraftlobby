package net.zumorik.hiso.listeners;

import net.zumorik.hiso.main.Lobby;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {

    private Lobby lobby;

    public PlayerMoveListener(Lobby lobby){
        this.lobby = lobby;
    }


    @EventHandler
    public void onMove(PlayerMoveEvent e){
        Player p = e.getPlayer();
        if(p.getLocation().getY() < 53){
            p.teleport(lobby.getLocationHandler().getLocation("Spawn"));
            p.playSound(p.getLocation(), Sound.NOTE_BASS, 1 ,1);
        }
    }

}
