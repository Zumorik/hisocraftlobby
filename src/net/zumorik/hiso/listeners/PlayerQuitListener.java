package net.zumorik.hiso.listeners;

import net.zumorik.hiso.main.Lobby;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    private Lobby lobby;

    public PlayerQuitListener(Lobby lobby){
        this.lobby = lobby;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        e.setQuitMessage(null);
        Player p = e.getPlayer();
        lobby.getLobbyScoreboards().removePlayer(p);
        lobby.getLobbyScoreboards().uploadOnline();
    }
}
