package net.zumorik.hiso.listeners.register;

import net.zumorik.hiso.listeners.*;
import net.zumorik.hiso.main.Lobby;
import org.bukkit.Bukkit;

public class RegisterListeners {

    private Lobby lobby;

    private PlayerBlockListener blocks;

    public RegisterListeners(Lobby lobby){
          this.lobby = lobby;
          setBlocks(new PlayerBlockListener(lobby));
    }

    public void registerListeners(){
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(lobby), lobby);
        Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(lobby), lobby);
        Bukkit.getPluginManager().registerEvents(new PlayerInventoryListener(lobby), lobby);
        Bukkit.getPluginManager().registerEvents(new PlayerMoveListener(lobby), lobby);
        Bukkit.getPluginManager().registerEvents(getBlocks(), lobby);
        Bukkit.getPluginManager().registerEvents(new PlayerHungerListener(), lobby);
        Bukkit.getPluginManager().registerEvents(new PlayerPVPListener(), lobby);
        Bukkit.getPluginManager().registerEvents(new DailyRewardsNPCListener(lobby), lobby);
        Bukkit.getPluginManager().registerEvents(new DailyRewardInventoryListener(lobby), lobby);
    }

    public PlayerBlockListener getBlocks() {
        return blocks;
    }

    public void setBlocks(PlayerBlockListener blocks) {
        this.blocks = blocks;
    }
}
