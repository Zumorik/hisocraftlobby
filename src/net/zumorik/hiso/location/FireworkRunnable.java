package net.zumorik.hiso.location;

import net.zumorik.hiso.commands.LobbyCommands;
import net.zumorik.hiso.main.Lobby;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftFirework;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class FireworkRunnable extends BukkitRunnable {

    int time = 9;
    int start = 1;

    private Lobby lobby;

    public FireworkRunnable(Lobby lobby){
        this.lobby = lobby;
    }

    @Override
    public void run() {

        if(start >= 9){
            cancel();
        }

        if(!LobbyCommands.enabled) {
            launchFireworkRed(lobby.getLocationHandler().getLocation(Integer.toString(start)));
            launchFireworkRed(lobby.getLocationHandler().getLocation(Integer.toString(start)));
            lobby.getLocationHandler().getLocation(Integer.toString(start)).getWorld()
                    .playEffect(lobby.getLocationHandler().getLocation(Integer.toString(start)), Effect.EXPLOSION_HUGE, 2);
        }else{
            launchFireworkGreen(lobby.getLocationHandler().getLocation(Integer.toString(start)));
            launchFireworkGreen(lobby.getLocationHandler().getLocation(Integer.toString(start)));
            lobby.getLocationHandler().getLocation(Integer.toString(start)).getWorld()
                    .playEffect(lobby.getLocationHandler().getLocation(Integer.toString(start)), Effect.EXPLOSION_HUGE, 2);
        }

        start++;


    }


    public void launchFireworkRed(Location loc) {

        Entity es = loc.getWorld().spawnEntity(loc.add(0, 0, 0), EntityType.FIREWORK);
        Firework f = (Firework) es;
        FireworkMeta fm = f.getFireworkMeta();
        Color c1 = Color.RED;
        fm.addEffect(FireworkEffect.builder().flicker(true).trail(false).with(FireworkEffect.Type.BALL_LARGE).withColor(c1)
                .withFade(Color.MAROON).build());
        fm.setPower(1);
        f.setFireworkMeta(fm);
        ((CraftFirework) f).getHandle().expectedLifespan = 1;
    }

    public void launchFireworkGreen(Location loc) {

        Entity es = loc.getWorld().spawnEntity(loc.add(0, 0, 0), EntityType.FIREWORK);
        Firework f = (Firework) es;
        FireworkMeta fm = f.getFireworkMeta();
        Color c1 = Color.LIME;
        fm.addEffect(FireworkEffect.builder().flicker(true).trail(false).with(FireworkEffect.Type.BALL_LARGE).withColor(c1)
                .withFade(Color.GREEN).build());
        fm.setPower(1);
        f.setFireworkMeta(fm);
        ((CraftFirework) f).getHandle().expectedLifespan = 1;
    }
}
