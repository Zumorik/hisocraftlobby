package net.zumorik.hiso.location;

import net.zumorik.hiso.main.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class LocationHandler {

    private Lobby lobby;

    public LocationHandler(Lobby lobby){
        this.lobby = lobby;
    }

    public void createLocation(String name, Player p){
        lobby.getConfig().set("locations." + name + ".x", p.getLocation().getX());
        lobby.getConfig().set("locations." + name + ".y", p.getLocation().getY());
        lobby.getConfig().set("locations." + name + ".z", p.getLocation().getZ());
        lobby.getConfig().set("locations." + name + ".yaw", p.getLocation().getYaw());
        lobby.getConfig().set("locations." + name + ".pitch", p.getLocation().getPitch());
        lobby.getConfig().set("locations." + name + ".world", p.getLocation().getWorld().getName());
        lobby.saveConfig();
    }

    public Location getLocation(String name){
        Location loc;

        double x = lobby.getConfig().getDouble("locations." + name + ".x");
        double y = lobby.getConfig().getDouble("locations." + name + ".y");
        double z = lobby.getConfig().getDouble("locations." + name + ".z");
        float yaw = (float) lobby.getConfig().getDouble("locations." + name + ".yaw");
        float pitch = (float) lobby.getConfig().getDouble("locations." + name + ".pitch");
        String world = lobby.getConfig().getString("locations." + name + ".world");

        loc = new Location(Bukkit.getWorld(world),x,y,z,yaw,pitch);

        return loc;
    }


}
