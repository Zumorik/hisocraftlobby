package net.zumorik.hiso.main;

import net.zumorik.hiso.commands.LobbyCommands;
import net.zumorik.hiso.inventory.ServerInventory;
import net.zumorik.hiso.language.LanguageStorage;
import net.zumorik.hiso.listeners.register.RegisterListeners;
import net.zumorik.hiso.location.LocationHandler;
import net.zumorik.hiso.npc.Holograms;
import net.zumorik.hiso.scoreboard.LobbyScoreboards;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class Lobby extends JavaPlugin {


    private LocationHandler locationHandler;
    private LobbyCommands lobbyCommands;
    private RegisterListeners registerListeners;
    private LobbyScoreboards lobbyScoreboards;
    private LanguageStorage languageStorage;
    private ServerInventory serverInventory;
    private Holograms holo;

    @Override
    public void onEnable(){
        saveDefaultConfig();
        setLanguageStorage(new LanguageStorage(this));
        setServerInventory(new ServerInventory(this));
        setRegisterListeners(new RegisterListeners(this));
        getRegisterListeners().registerListeners();
        setLocationHandler(new LocationHandler(this));
        setLobbyCommands(new LobbyCommands(this));
        setLobbyScoreboards(new LobbyScoreboards(this));
        setHolo(new Holograms(this));
        getHolo().spawnHolograms();
        getCommand("hiso").setExecutor(getLobbyCommands());
        getServer().getConsoleSender().sendMessage(colo("&e&lHiso&f&lCraft &7&l: &bLobby plugin has been enabled."));
    }

    @Override
    public void onDisable(){
        saveConfig();
        getServer().getConsoleSender().sendMessage(colo("&e&lHiso&f&lCraft &7&l: &cLobby has been disabled."));
    }


    public Holograms getHolo(){
        return holo;
    }

    public void setHolo(Holograms holo) {
        this.holo = holo;
    }

    public ServerInventory getServerInventory() {
        return serverInventory;
    }

    public void setServerInventory(ServerInventory serverInventory) {
        this.serverInventory = serverInventory;
    }

    public LanguageStorage getLanguageStorage() {
        return languageStorage;
    }

    public void setLanguageStorage(LanguageStorage languageStorage) {
        this.languageStorage = languageStorage;
    }

    public LobbyScoreboards getLobbyScoreboards() {
        return lobbyScoreboards;
    }

    public void setLobbyScoreboards(LobbyScoreboards lobbyScoreboards) {
        this.lobbyScoreboards = lobbyScoreboards;
    }

    public LobbyCommands getLobbyCommands() {
        return lobbyCommands;
    }

    public void setLobbyCommands(LobbyCommands lobbyCommands) {
        this.lobbyCommands = lobbyCommands;
    }

    public LocationHandler getLocationHandler() {
        return locationHandler;
    }

    public void setLocationHandler(LocationHandler locationHandler) {
        this.locationHandler = locationHandler;
    }

    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public RegisterListeners getRegisterListeners() {
        return registerListeners;
    }

    public void setRegisterListeners(RegisterListeners registerListeners) {
        this.registerListeners = registerListeners;
    }
}
