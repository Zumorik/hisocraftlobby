package net.zumorik.hiso.npc;

import net.zumorik.hiso.main.Lobby;
import net.zumorik.main.Core;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

public class Holograms {

    private Lobby core;

    public Holograms(Lobby core){
        this.core = core;
    }

    public void spawnHolograms(){

        Location loc =  core.getLocationHandler().getLocation("hologram1");

        for(Entity e : loc.getWorld().getEntities()){
            if(e instanceof ArmorStand){
                e.remove();
            }
        }

        ArmorStand armor = loc.getWorld().spawn(loc.clone().add(0,2.5,0), ArmorStand.class);
        armor.setGravity(false);
        armor.setCustomNameVisible(true);
        armor.setMarker(true);
        armor.setVisible(false);
        armor.setCustomName(colo("&a&lNEW! &e&lSkyWars"));

        ArmorStand armor1 = loc.getWorld().spawn(loc.clone().add(0,2,0), ArmorStand.class);
        armor1.setGravity(false);
        armor1.setVisible(false);
        armor1.setMarker(true);
        armor1.setCustomNameVisible(true);
        armor1.setCustomName(colo("&6Online&8: &aNULL"));

        Location loc1 =  core.getLocationHandler().getLocation("hologram2");
        ArmorStand armor2 = loc1.getWorld().spawn(loc1.clone().add(0,2.5,0), ArmorStand.class);
        armor2.setGravity(false);
        armor2.setVisible(false);
        armor2.setMarker(true);
        armor2.setCustomNameVisible(true);
        armor2.setCustomName(colo("&A&lNEW! &e&lBedWars"));

        ArmorStand armor3 = loc1.getWorld().spawn(loc1.clone().add(0,2,0), ArmorStand.class);
        armor3.setGravity(false);
        armor3.setCustomNameVisible(true);
        armor3.setMarker(true);
        armor3.setVisible(false);
        armor3.setCustomName(colo("&6Online&8: &aNULL"));

        Location loc2 =  core.getLocationHandler().getLocation("hologram3");
        ArmorStand armor4 = loc2.getWorld().spawn(loc2.clone().add(0,2.5,0), ArmorStand.class);
        armor4.setGravity(false);
        armor4.setVisible(false);
        armor4.setMarker(true);
        armor4.setCustomNameVisible(true);
        armor4.setCustomName(colo("&a&lNEW! &e&lPractivePvP"));

        ArmorStand armor5 = loc2.getWorld().spawn(loc2.add(0,2,0), ArmorStand.class);
        armor5.setGravity(false);
        armor5.setVisible(false);
        armor5.setCustomNameVisible(true);
        armor5.setMarker(true);
        armor5.setCustomName(colo("&6Online&8: &aNULL"));

        Location loc3 =  core.getLocationHandler().getLocation("hologram4");
        ArmorStand armor6 = loc3.getWorld().spawn(loc3.clone().add(0,2.5,0), ArmorStand.class);
        armor6.setGravity(false);
        armor6.setMarker(true);
        armor6.setVisible(false);
        armor6.setCustomNameVisible(true);
        armor6.setCustomName(colo("&a&lDaily Rewards"));

        ArmorStand armor7 = loc3.getWorld().spawn(loc3.add(0,2,0), ArmorStand.class);
        armor7.setGravity(false);
        armor7.setVisible(false);
        armor7.setMarker(true);
        armor7.setCustomNameVisible(true);
        armor7.setCustomName(colo("&6&lClick to Claim"));

        Location loc4 =  core.getLocationHandler().getLocation("hologram5");
        ArmorStand armor8 = loc4.getWorld().spawn(loc4.clone().add(0,2.5,0), ArmorStand.class);
        armor8.setGravity(false);
        armor8.setVisible(false);
        armor8.setCustomNameVisible(true);
        armor8.setMarker(true);
        armor8.setCustomName(colo("&a&lHelper"));

        ArmorStand armor9 = loc4.getWorld().spawn(loc4.add(0,2,0), ArmorStand.class);
        armor9.setGravity(false);
        armor9.setVisible(false);
        armor9.setCustomNameVisible(true);
        armor9.setMarker(true);
        armor9.setCustomName(colo("&aServer Tutorial"));
    }

    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }
}
