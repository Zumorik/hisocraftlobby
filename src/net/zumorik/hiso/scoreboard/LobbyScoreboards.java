package net.zumorik.hiso.scoreboard;

import java.sql.SQLException;

import net.zumorik.hiso.main.Lobby;
import net.zumorik.hiso.util.SB;
import net.zumorik.main.api.CoreAPI;
import net.zumorik.main.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class LobbyScoreboards {

    private Lobby lobby;
    public LobbyScoreboards(Lobby lobby){
        this.lobby = lobby;
    }

    public void setupScoreboard(Player p) {
        if (!StoredScores.boards.containsKey(p.getName())) {
            StoredScores.boards.put(p.getName(), new SB(p, "&e&lHiso&f&lCraft"));
            ((SB) StoredScores.boards.get(p.getName())).send(p);
            try {
                update((SB) StoredScores.boards.get(p.getName()), p);
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                update((SB) StoredScores.boards.get(p.getName()), p);
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void removePlayer(Player p) {
        if (StoredScores.boards.containsKey(p.getName())) {
            StoredScores.boards.remove(p.getName());
        }
    }

    public void uploadOnline() {
        int size = Bukkit.getOnlinePlayers().size();
        for (Player ps : Bukkit.getOnlinePlayers()) {
            if (StoredScores.boards.containsKey(ps.getName())) {
                SB board = StoredScores.boards.get(ps.getName());
                if(lobby.getLanguageStorage().useEnglish(ps)){
                    board.setScore(3, "&fPlayers: &b" + size);
                }else {
                    board.setScore(3, "&fJugadores: &b" + size);
                }
            }
        }
    }

    public void updateCoins(Player p) throws SQLException, ClassNotFoundException {
        int coins = CoreAPI.getEconomy().getCoins(p);
                SB board = StoredScores.boards.get(p.getName());
                if(lobby.getLanguageStorage().useEnglish(p)){
                    board.setScore(7, "&fGems: &a" + coins);
                }else {
                    board.setScore(7, "&fGemas: &a" + coins);
                }
    }

    public void update(SB LobbyBoard, Player p) throws ClassNotFoundException, SQLException {

        if(lobby.getLanguageStorage().useEnglish(p)){

            int size = Bukkit.getOnlinePlayers().size();
            LobbyBoard.setScore(15, "&f");
            LobbyBoard.setScore(14, "&b&l" + p.getName());
            LobbyBoard.setScore(9, "&f&e");
            LobbyBoard.setScore(8, "&fRank: &a&cloading...");
            LobbyBoard.setScore(7, "&fGems: &a&cloading...");
            LobbyBoard.setScore(6, "&fLevel: &a&cloading...");
            LobbyBoard.setScore(5, "&f&e&r&e");
            LobbyBoard.setScore(4, "&fLobby: &b#1");
            LobbyBoard.setScore(3, "&fPlayers: &b" + size);
            LobbyBoard.setScore(2, "&f&r&l");
            LobbyBoard.setScore(1, "&e&lmc.hisocraft.net");

            Bukkit.getScheduler().runTaskAsynchronously(lobby, () ->{
                        int wins = CoreAPI.getStats().getSkyWarsWins(p);
                        int kills = CoreAPI.getStats().getSkyWarsKills(p);
                        int coins = CoreAPI.getEconomy().getCoins(p);
                        int deaths = CoreAPI.getStats().getSkyWarsDeaths(p);
                        int lvl = CoreAPI.getStats().getLevel(p);
                        String rank = CoreAPI.getCore().getRankDatabase().getRank(p);
                        LobbyBoard.setScore(15, "&f");
                        LobbyBoard.setScore(14, "&b&l" + p.getName());
                        LobbyBoard.setScore(9, "&f&e");
                        LobbyBoard.setScore(8, "&fRank: "+ getRankColor(p, rank) + rank);
                        LobbyBoard.setScore(7, "&fGems: &a" + coins);
                        LobbyBoard.setScore(6, "&fLevel: &a" + lvl);
                        LobbyBoard.setScore(5, "&f&e&r&e");
                        LobbyBoard.setScore(4, "&fLobby: &b#1");
                        LobbyBoard.setScore(3, "&fPlayers: &b" + size);
                        LobbyBoard.setScore(2, "&f&r&l");
                        LobbyBoard.setScore(1, "&e&lmc.hisocraft.net");

            });

        }else {

            int size = Bukkit.getOnlinePlayers().size();
            LobbyBoard.setScore(15, "&f");
            LobbyBoard.setScore(14, "&b&l" + p.getName());
            LobbyBoard.setScore(9, "&f&e");
            LobbyBoard.setScore(8, "&fRango: &a&cloading...");
            LobbyBoard.setScore(7, "&fGemas: &a&cloading...");
            LobbyBoard.setScore(6, "&fNivel: &a&cloading...");
            LobbyBoard.setScore(5, "&f&e&r&e");
            LobbyBoard.setScore(4, "&fLobby: &b#1");
            LobbyBoard.setScore(3, "&fJugadores: &b" + size);
            LobbyBoard.setScore(2, "&f&r&l");
            LobbyBoard.setScore(1, "&e&lmc.hisocraft.net");

            Bukkit.getScheduler().runTaskAsynchronously(lobby, () -> {
                        int wins = CoreAPI.getStats().getSkyWarsWins(p);
                        int kills = CoreAPI.getStats().getSkyWarsKills(p);
                        int coins = CoreAPI.getEconomy().getCoins(p);
                        int deaths;
                        deaths = CoreAPI.getStats().getSkyWarsDeaths(p);
                        int lvl = CoreAPI.getStats().getLevel(p);
                        String rank = CoreAPI.getCore().getRankDatabase().getRank(p);
                        LobbyBoard.setScore(15, "&f");
                        LobbyBoard.setScore(14, "&b&l" + p.getName());
                        LobbyBoard.setScore(9, "&f&e");
                        LobbyBoard.setScore(8, "&fRango: " + getRankColor(p, rank) + rank);
                        LobbyBoard.setScore(7, "&fGemas: &a" + coins);
                        LobbyBoard.setScore(6, "&fNivel: &a" + lvl);
                        LobbyBoard.setScore(5, "&f&e&r&e");
                        LobbyBoard.setScore(4, "&fLobby: &b#1");
                        LobbyBoard.setScore(3, "&fJugadores: &b" + size);
                        LobbyBoard.setScore(2, "&f&r&l");
                        LobbyBoard.setScore(1, "&e&lmc.hisocraft.net");

            });

        }
    }

    public String getRankColor(Player p, String rank){
        String color = "&e";

        rank = rank.toUpperCase();
        Rank r = Rank.valueOf(rank);

        switch(r){

            case BUILDER:
            case ELITE:
            case EPIC:
                color = "&b";
                break;
            case YOUTUBER:
                color = "&d";
                break;
            case ULTRA:
                color = "&6";
                break;
            case ADMIN:
                color = "&c";
                break;
            case OWNER:
                color = "&4";
                break;
            case MOD:
                color = "&2";
                break;
            case VIP:
                color = "&a";
                break;
            case STREAMER:
                color = "&3";
                break;
        }

        return color;
    }

}
