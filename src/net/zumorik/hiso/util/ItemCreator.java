package net.zumorik.hiso.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemCreator {

    private ItemStack itemStack;
    private ItemMeta itemMeta;
    private boolean enchant = false;

    private List<String> lores = new ArrayList<>();

    public ItemCreator(ItemStack itemStack) {
        this.itemStack = itemStack;
        this.itemMeta = itemStack.getItemMeta();
    }

    public ItemStack toItemStack() {
        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        itemMeta.setLore(lores);
        itemStack.setItemMeta(itemMeta);
        if (enchant) {
            itemStack.addUnsafeEnchantment(Enchantment.OXYGEN, 1);
        }
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        return itemStack;
    }

    public ItemCreator setEnchanted(boolean b) {
        this.enchant = b;
        return this;
    }

    public ItemCreator setName(String name) {
        itemMeta.setDisplayName(colorize(name));
        return this;
    }

    public ItemCreator lore(String lore) {
        lores.add(colorize(lore));
        return this;
    }

    public ItemCreator lores(String... lores) {
        for (String lore : lores) {
            lore(lore);
        }
        return this;
    }

    public ItemCreator setUnbreakable(boolean value) {
        itemMeta.spigot().setUnbreakable(value);
        return this;
    }

    private String colorize(String str) {
        return ChatColor.translateAlternateColorCodes('&', str);
    }

    public ItemCreator lores(List<String> unlocked) {
        for (String s : unlocked) {
            lores.add(s);
        }
        return this;
    }

}